import React, { useState } from "react";
import "./App.css";
import axios from "axios";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const BOT_TOKEN = "6847593670:AAFVeERWr73gBsZ2cCPf0aXt63nsLEk0uxA"; // Bot tokeni yoziladi
const CHAT_ID = "-1001834887573"; // Habar yuborish kerak bo'lgan chat Idsi yoziladi

function App() {
  const [inputValue, setInputValue] = useState({
    name: "",
    phone: "",
    email: "",
  });
  const SENDING_TEXT = JSON.stringify(inputValue).replace(
    `<b>Name:</b> ${inputValue.name}\n
    <b>Phone:</b> ${inputValue.phone}\n 
    <b>Email:</b> ${inputValue.email}`
  );
  const url = `https://api.telegram.org/bot${BOT_TOKEN}/sendMessage?chat_id=${CHAT_ID}&parse_mode=html&text=${SENDING_TEXT}`;
  const handleChange = (e) => {
    const { name, value } = e.target;
    setInputValue({ ...inputValue, [name]: value });
  };
  const hendleSubmit = (e) => {
    e.preventDefault();
    if (
      inputValue.name === "" ||
      inputValue.phone === "" ||
      inputValue.email === ""
    ) {
      toast.error("Iltimos barcha maydonni to'ldiring");
    } else {
      axios
        .post(url)
        .then((r) => toast.success("Xabaringiz yuborildi"))
        .catch((err) => toast(err.message))
        .finally(() => setInputValue({ name: "", phone: "", email: "" }));
    }
  };
  return (
    <div className="App">
      <header className="App-header">
        <form onSubmit={hendleSubmit}>
          <label htmlFor="name"> Ismingingiz:</label>
          <input
            id="tgbot"
            className=""
            type="text"
            name="name"
            value={inputValue.name}
            onChange={handleChange}
          />
          <br />
          <label htmlFor="phone"> Raqamingiz:</label>
          <input
            id="tgbot"
            type="text"
            name="phone"
            value={inputValue.phone}
            onChange={handleChange}
          />
          <br />
          <label htmlFor="email"> Electron Manzili:</label>
          <input
            id="tgbot"
            type="email"
            name="email"
            value={inputValue.email}
            onChange={handleChange}
          />
          <br />
          <button type="submit">Yuborish</button>
        </form>
        <ToastContainer />
      </header>
    </div>
  );
}

export default App;
